const DEV_MODE = ( process . env . NODE_ENV !== 'production' ) ;

import express from 'express' ;
import server from './app.js' ;

const morgan = ( DEV_MODE ? require ( 'morgan' ) : null ) ;

var app = express ( ) ;
const port = process . env . PORT || '3000' ;

if ( morgan ) { app . use ( morgan ( 'dev' ) ) ; }
app . use ( '/dark' , server ) ;
app . get ( '/' , dark_redirect ) ;
app . get ( '/*' , dark_redirect ) ;
app . listen ( port ) ;

function dark_redirect ( req , res ) {
	res . redirect ( '/dark' ) ;
}
