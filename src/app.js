import path	from 'path'	;
import express	from 'express'	;

var app = express ( )	;

app . set ( 'view engine' , 'pug' ) ;

app . set ( 'views' , path . join ( __dirname , 'views' ) ) ; 
app . use ( ( req , res , next ) => {
	res.setHeader ( 'Cache-Control' , 'no-cache, no-store, must-revalidate' ) ;
	res.setHeader ( 'Pragma' , 'no-cache' ) ;
	res.setHeader ( 'Expires' , 0 ) ;
	next ( ) ;
} ) ;
app . use ( '/public' , express . static ( __dirname ) ) ;
app . use ( ( req , res , next ) => {
	res.setHeader ( 'Cache-Control' , 'no-cache, no-store, must-revalidate' ) ;
	res.setHeader ( 'Pragma' , 'no-cache' ) ;
	res.setHeader ( 'Expires' , 0 ) ;
	next ( ) ;
} ) ;

app . use ( express . urlencoded ( { extended : false } ) ) ;

// routes
app . get ( '/documentation' , ( req , res ) => {
	res . render ( 'documentation' ) ;
} ) ;
app . get ( '/layout-fixed-1' , ( req , res ) => {
	res . render ( 'layout1' ) ;
} ) ;
app . get ( '/' , test_redirect ) ;

export default app ;

function test_redirect ( req , res ) {
	res . redirect ( '/dark/documentation' ) ;
}
