import gulp from 'gulp' ;
import { series , parallel , task , watch } from 'gulp' ;
import { APP_DATA , MODE , PATH } from './task/VAR' ;

let plugins = { } ;
// let plugins = require ( 'gulp-load-plugins' ) ( ) ;

plugins . babel = require ( 'gulp-babel' ) ;
plugins . inject = require ( 'gulp-inject' ) ;
plugins . postcss = require ( 'gulp-postcss' ) ;
plugins . pug = require ( 'gulp-pug' ) ;
plugins . noop = require ( 'gulp-noop' ) ;
plugins . sourcemaps = require ( 'gulp-sourcemaps' ) ;
// plugins . sourcemaps = require ( 'gulp-sourcemaps' ) ;

plugins . terser = require ( 'gulp-terser' ) ;
plugins . stripDebug = require ( 'gulp-strip-debug' ) ;
plugins . nodemon = require ( 'gulp-nodemon' );

plugins . sass = require('gulp-sass')(require('node-sass'));
plugins . cssnano	= require ( 'cssnano' ) ;
plugins . autoprefixer	= require ( 'autoprefixer' ) ;
plugins . browserify	= require ( 'browserify' ) ;
plugins . babelify	= require ( 'babelify' ) ;
plugins . buffer	= require ( 'vinyl-buffer' ) ;
plugins . sourceStream	= require ( 'vinyl-source-stream' ) ;
// plugins . watchify	= require ( 'watchify' ) ;

const getTask = task => require ( './task/' + task ) ( gulp , plugins ) ;
// First Order Tasks
task ( 'views'		, getTask	( 'views'			) ) ;
task ( 'server'		, getTask	( 'server'			) ) ;
task ( 'styles'		, getTask	( 'styles'			) ) ;
task ( 'scripts'	, getTask	( 'scripts'			) ) ;
task ( 'pre-scripts'	, getTask	( 'pre-scripts'			) ) ;
task ( 'startNodemon'	, getTask	( 'startNodemon'		) ) ;

// Second Order Tasks
task ( 'client'	, series	( 'styles' , 'scripts' , 'pre-scripts' , 'views' ) ) ;
task ( 'watchClient'	, ( ) => watch (
	[
		PATH . VIEWS ,
		'src' ,
	] ,
	series ( 'client' )
) ) ;

// Third Order Tasks
task ( 'serve'		, series	( 'client'	, 'server'	, 'startNodemon'	) ) ;
// Fourth Order Tasks
task ( 'test'		, parallel ( 'serve' , 'watchClient' ) ) ;

task ( 'default'	, series ( 'test' ) ) ;
