# dark

Dark bootstrap theme.
To use the entire bootstrap theme just import src/darks.scss.
If you want specific stuff you can just import what you want from the src.

## NPM Commands:

	// Builds dark. Then, starts documentation on a server with debug tools at localhost:4000/test.
	npm run test
	// builds the server and spits out the file as dark.css in build.
	npm run prod-build
	// starts the documentation on a server
	npm run prod-start

## Work To Do:

1. Create custom carousel
