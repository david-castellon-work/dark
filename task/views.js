import { PATH } from './VAR' ;

let { STYLE_IN , SCRIPT_IN , VIEW_IN } = PATH ;

export default ( gulp , plugins ) => ( ) => {
	return gulp . src ( VIEW_IN )
		. pipe ( gulp . dest ( 'build/views' ) ) ;
}
