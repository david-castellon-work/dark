import { MODE , PATH } from './VAR' ;
export default ( gulp , plugins ) => ( ) =>
	gulp . src ( PATH . SERVER_IN )
	. pipe ( MODE . DEV ? plugins . sourcemaps . init ( ) : plugins . noop ( ) )
	. pipe ( plugins . babel ( {
		presets : [ '@babel/preset-env' ]
	} ) )
	. pipe ( plugins . terser ( ) )
	. pipe ( MODE . DEV ? plugins . stripDebug ( ) : plugins . noop ( ) )
	. pipe ( MODE . DEV ? plugins . sourcemaps . mapSources ( function ( sourcePath , file ) {
		return 'src/' + sourcePath ;
	} ) : plugins . noop ( ) )
	. pipe ( MODE . DEV ? plugins . sourcemaps . write ( './maps' , {
		includeContent : false
	} ) : plugins . noop ( ) )
	. pipe ( gulp . dest ( 'build' ) ) ;
