
import { PATH , MODE } from './VAR' ;

let { SCRIPTS_IN , SCRIPTS_OUT } = PATH ;
export default ( gulp , plugins ) => ( ) =>
	plugins . browserify ( {
		entries : SCRIPTS_IN ,
		fullPaths : true ,
		debug : true ,
		sourceMaps : true ,
		cache : { } ,
		packageCache : { } ,
	} )
	. transform ( 'babelify' , {
		global : true ,
		// transform all files that are imported
		// only : [ /\/node_modules\/bootstrap\// ] ,
		// do not transform the files that match with the regex
		// the regex is all files except bootstrap. so it should
		// ignore everything except bootstrap
		// src/components/password-toggle/password-toggle.js
		ignore : [ /\/node_modules\/(?!bootstrap)\// , ] ,
		// ignore : [ /node_modules\/(?!bootstrap)/ ] ,
		// ignore : [ /node_modules/ ] ,
		// ignore : [ 'src/dark.js' , 'src/test.js' ] ,
		// ignore : [ /(?!\/src\/dark.js)|(\/node_modules\/(?!bootstrap))/ ] ,
		presets : [
			[
				'@babel/env' ,
				{
					loose : true ,
					// modules : false ,
					exclude: [ 'transform-typeof-symbol' ] ,
				}
			] ,
		] ,
		plugins : [
			'@babel/plugin-proposal-object-rest-spread' ,
		] ,
	} )
	. bundle ( )
	. on ( 'error' , err => console.log ( 'Browserify Error: ' + err.message ) )
	. pipe ( plugins.sourceStream( SCRIPTS_OUT ) )
	. pipe ( plugins.buffer( ) )
	. pipe ( plugins.sourcemaps . init ( { loadMaps : true } ) )
	. pipe ( plugins.terser ( ) )
	. pipe ( plugins.sourcemaps . write ( './maps' ) )
	. pipe ( gulp.dest ( 'build' ) ) ;
